# Puissance 4 en ligne de commandes !
Un Puissance 4 en python dans la ligne de commande fait par
- Alexis
- Emeric
- Nicolas
- Amine
Réalisé & présenté dans le cadre d'un projet scolaire.

## Comment installer et jouer ?
Assurez-vous d'avoir installé **Python** version 3.6 ou supérieur
**Télécharger** le fichier `connect4.py` ou téléchargez le projet entier (choisissez .zip)
Une fois le fichier téléchargé, **executez-le dans un terminal** avec `python connect4.py`
Vous pouvez **jouer au jeu** en suivant les **instructions inscrites dans le terminal**.

## License :
La permission vous est accordée d'**utiliser, de modifier et de republier** ce logiciel comme bon vous semble du moment que :
- Toute modification apportée soit aussi **accessible au public sous cette license**
- Vous gardez le **crédit des auteurs originaux** au début du code source et dans d'éventuels crédits
*Le groupe d'auteur original se réserve tout droits d'auteur sur ce programme.*

## Envie de contribuer ? Vous avez trouvé un bug ?
Ce dépot de code est une simple archive mais n'hesitez pas à le modifier et le republier.
Soyez indulgents sur la qualité du code. Tout est commenté pour faciliter la compréhension.
