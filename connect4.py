# PUISSANCE 4 - Edition ligne de commande !
# Copyright 2023 - Groupe NSI LPBH
# Tout droits réservés

YELLOW = "jaune" # Valeur numérique : 1
RED = "rouge" # valeur numérique : 2

print("Bienvenue dans la version 'ligne de commande' de Puissance 4!")
print("---------- Règles du jeu ----------")
print("O = jaune, X = rouge, _ = case vide ")
print("Le premier à faire un aligement (horizontal, vertical ou diagonal)\r\nd'au moins quatre jetons de sa couleur gagne la partie.")
print("Chacun son tour, le joueur sélectionne une colonne pour poser un de ses jetons.")
print("--------- A vous de jouer ---------")

board = []
playerTurn = YELLOW # yellow et red sont des tours valides

def createBoard():
    for i in range(7):
        board.append([0, 0, 0, 0, 0, 0])

def changeTurn():
    global playerTurn
    if playerTurn == YELLOW:
        playerTurn = RED
    else:
        playerTurn = YELLOW



def cellToString(cell):
    if cell == 0:
        return " _ |"
    if cell == 1:
        return "\u001b[43m O \u001b[00m|"
    if cell == 2:
        return "\u001b[41m X \u001b[00m|"

def printBoard():
    print('   | 1 | 2 | 3 | 4 | 5 | 6 | 7 |')

    # On stocke le texte à mettre devant chaque ligne :
    stringRows = ["1: |", "2: |", "3: |", "4: |", "5: |", "6: |"]
    
    # Ici on récupère les colonnes et on itère dessus

    for col in board:
        row = 0
        # Ensuite pour chaque cellule de la colonne col, on ajoute un texte représenant
        # l'état de la cellule dans la ligne actuelle pour ensuite l'imprimer
        for cell in col:
            stringRows[row] += cellToString(cell)
            row += 1
    
    # On imprime ensuite ligne par ligne
    for printableRow in stringRows:
        print(printableRow)


def check_token_position(column, idx) -> bool:
    # Premièrement, on vérifie si on a atteint la fin de la colone
    # Puis, si la cellule en dessous est libre
    # on renvoie faux, sinon on renvoie vrai
    if idx == len(column)-1:
        return True
    elif column[idx+1] != 0:
        return True
    else:
        return False
    
def check_vertical_victory(x, y, player):
    """
    Vérification de l'alignement sur l'axe vertical
    """

    blockedFromUp = False
    upperNeighbor = 0
    downerNeighbor = 0
    blockedFromDown = False
    column = board[x]
    tokensAlignedCount = 1

    for offset in range(1, 6):
        # Pour gérer les cases au dessus
        if not blockedFromUp:
            if y - offset <= 0:
                blockedFromUp = True
            else:
                upperNeighbor = column[y-offset]
            # Vérification de la case au dessus
            if upperNeighbor != player:
                blockedFromUp = True
        # Pour gérer les cases en dessous
        if not blockedFromDown:
            if y+offset > 5:
                blockedFromDown = True
            else:
                downerNeighbor = column[y+offset]
                # Vérification de la case en dessous
                if downerNeighbor != player:
                    blockedFromDown = True
                else:
                    tokensAlignedCount += 1
        if blockedFromUp and blockedFromDown:
            break
    if tokensAlignedCount >= 4:
        return (True, tokensAlignedCount) # Joueur a gagné
    else:
        return (False, 0)
    
def check_horizontal_victory(x, y, player):
    """
    Vérification de l'alignement horizontal
    """

    blockedFromLeft = False
    blockedFromRight = False
    lefterNeighbor = 0
    righterNeighbor = 0
    tokensAlignedCount = 1
    line = []
    # On récupère chaque cellule à la bonne positon X sur chaque colonne (récupérer la ligne)
    for k in range(7):
        line.append(board[k][y])
        
    for offset in range(1, 7):
        # Gérer les cases à gauche
        if not blockedFromLeft:
            if x-offset <= 0:
                blockedFromLeft = True
            else:
                lefterNeighbor = line[x-offset]
            if lefterNeighbor != player:
                blockedFromLeft = True
            else:
                tokensAlignedCount += 1
        # Gérer les cases à droite
        if not blockedFromRight:
            if x+offset >= 6:
                blockedFromRight = True
            else:
                righterNeighbor = line[x+offset]
            if righterNeighbor != player:
                blockedFromRight = True
            else:
                tokensAlignedCount += 1
        if blockedFromLeft and blockedFromRight:
            break

    if tokensAlignedCount >= 4:
        return (True, tokensAlignedCount) # Joueur a gagné
    else:
        return (False, 0)

def check_diagonal1_victory(x, y, player):
    tokensAlignedCount = 1
    blockedFromLeft = False
    blockedFromRight = False
    
    # On traverse le tableau de manière diagonale (direction : \) et on compte les jetons que l'on rencontre
    for offset in range(1, 7):
        if not blockedFromLeft:
            if x - offset >= 0 and y - offset >= 0:
                # Prendre cellule du dessus gauche
                lefterNeighbor = board[x-offset][y-offset]
                if lefterNeighbor != player:
                    blockedFromLeft = True
                else:
                    tokensAlignedCount += 1
            else:
                blockedFromLeft = True

        if not blockedFromRight:
            if x + offset < 7 and y + offset < 6:
                # Prendre la cellule du dessous droite
                righterNeighbor = board[x+offset][y+offset]
                if righterNeighbor != player:
                    blockedFromRight = True
                else:
                    tokensAlignedCount += 1
            else:
                blockedFromRight = True
        if blockedFromLeft and blockedFromRight:
            break
    if tokensAlignedCount >= 4:
        return (True, tokensAlignedCount) # Joueur a gagné
    else:
        return (False, 0)
    
def check_diagonal2_victory(x, y, player):
    tokensAlignedCount = 1
    blockedFromLeft = False
    blockedFromRight = False

    # On traverse le tableau diagonalement (direction : /) et on compte les jetons sur l'axe
    for offset in range(1, 7):
        if not blockedFromLeft:
            if x - offset >= 0 and y + offset < 6:
                # Récupérer la cellule en bas à droite
                lefterNeighbor = board[x - offset][y + offset]
                if lefterNeighbor != player:
                    blockedFromLeft = True
                else:
                    tokensAlignedCount += 1 
            else:
                blockedFromLeft = True
        
        if not blockedFromRight:
            if x + offset < 7 and y - offset >= 0:
                # Récupérer la cellule en haut à gauche
                righterNeighbor = board[x + offset][y - offset]
                if righterNeighbor != player:
                    blockedFromRight = True
                else:
                    tokensAlignedCount += 1 
            else:
                blockedFromRight = True
        if blockedFromLeft and blockedFromRight:
            break
    if tokensAlignedCount >= 4:
        return (True, tokensAlignedCount) # Joueur a gagné
    else:
        return (False, 0)

def checkVictoryAt(x, y, player) -> (bool, int):
    """
    Checks for a victory for either yellow (1) or red (2)
    based on a position of a token. This is a "discrete" way
    Vérifie si il y a une victoire pour jaune (1) ou rouge (2)
    basé sur la position du palet posé. C'est une manière "discrète"
    """

    # Vérifie l'alignement vertical des palets (on lit sur la même colonne)
    win, count = check_vertical_victory(x, y, player)
    if win:
        return (win, count)
    # Vérifie si on a un alignement horizontal (lecture de gauche à droite)
    win, count = check_horizontal_victory(x, y, player)
    if win:
        return (win, count)
    # Vérifier la diagonale (haut gauche -> bas droite)
    win, count = check_diagonal1_victory(x, y, player)
    if win:
        return (win, count)
    # Vérifie la diagonale (haut droite -> bas gauche)
    win, count = check_diagonal2_victory(x, y, player)
    if win:
        return (win, count)
    
    # Si on atteint cet endroit, on est sur que personne n'a encore gagné
    return (False, 0)


# Fonction qui gère l'ajout d'un jeton
def addNewToken(column, colidx):
    for idx in range(len(column)):
        hasReachEnd = check_token_position(column, idx)
        if hasReachEnd:
            if playerTurn == YELLOW:
                board[colidx][idx] = 1
            else:
                board[colidx][idx] = 2
            break
    else:
        if playerTurn == YELLOW:
            board[colidx][idx] = 1
        else:
            board[colidx][idx] = 2
        pass
    if playerTurn == YELLOW:
        return checkVictoryAt(colidx, idx, 1)
    if playerTurn == RED:
        return checkVictoryAt(colidx, idx, 2)

def check_for_full_columns():
    """
    Vérifie si toutes les colonnes sont pleines
    Checks if every column is full
    """
    fullCols = 0
    for col in board:
        if col[0] != 0:
            fullCols+=1
    return fullCols == 7

hasToExit = False

def handleUserInput():
    """
    Handles user's inputs
    Gère les entrées de utilisateur
    """
    global hasToExit
    boardFull = check_for_full_columns()
    if boardFull:
        hasToExit = True
        print("------------ RECAP DE LA PARTIE ------------")
        print("Match nul ! Toute les colonnes sont pleines.")
        print("Fin de la partie ! Pas de gagnant...")
        print("----------- MERCI D'AVOIR JOUÉ ! -----------")
        exit(0)
    
    if hasToExit:
        return
    userInput = input("Choisissez une colonne [" + playerTurn + "] > ")
    parsedUserInput = 0
    if userInput == "abandon" or userInput == "quitter" :
        print("------------ RECAP DE LA PARTIE ------------")
        print(playerTurn + "A décidé d'abandonner. Fin de partie")
        changeTurn()
        print("Fin de la partie ! Gagnant : " + playerTurn + ". Bravo !")
        print("----------- MERCI D'AVOIR JOUÉ ! -----------")
        hasToExit = True
        exit(0) # Apparently Python doesn't let us exit for some reason
        
    if hasToExit:
        exit(0)

    try:
        parsedUserInput = int(userInput)
    except:
        print("Veuillez entrer un nombre valide")
        parsedUserInput = 0
        handleUserInput()
    finally:
        if parsedUserInput < 1 or parsedUserInput > 7:
            print("Selectionnez une colonne entre 1 et 7.\r\nVous avez tenté : " + str(parsedUserInput)+"." )
            parsedUserInput = 0
            handleUserInput()
        else:
            if board[parsedUserInput-1][0] != 0:
                print("La colonne choisie (" + str(parsedUserInput)+") est pleine.\r\nChoisissez-en une autre SVP." )
                handleUserInput()
            winned, alignedTokens = addNewToken(board[parsedUserInput-1], parsedUserInput-1)
            
            
            printBoard()
            if winned:
                print("------------ RECAP DE LA PARTIE ------------")
                print(playerTurn + " a aligné " + str(alignedTokens) + " palets & gagne !")
                print("Fin de la partie ! Gagnant : " + playerTurn + ". Bravo !")
                print("----------- MERCI D'AVOIR JOUÉ ! -----------")
                hasToExit = True
                exit(0)
                
            changeTurn()
            if not hasToExit:
                print('C\'est au tour de : ' + playerTurn)
                handleUserInput()
            else:
                return


createBoard()
printBoard()
handleUserInput()
exit(0)